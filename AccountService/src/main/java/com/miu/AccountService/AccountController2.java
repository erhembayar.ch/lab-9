package com.miu.AccountService;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("Two")
public class AccountController2 {

    @GetMapping("/account/{customerid}")
    public String getName(@PathVariable("customerid") String customerId){
        return "Account 2: " + customerId;

    }
}